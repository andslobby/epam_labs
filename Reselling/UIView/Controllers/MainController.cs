﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using System;
using System.Linq;

namespace Resaling.Controllers
{
    public class MainController : Controller
    {

        IRepository _repo;
        readonly int _eventsPageSize = 4;
        readonly int _ticketsForEventsPageSize = 4;


        public MainController(IRepository repo)
        {
            _repo = repo;
        }

        public IActionResult Events(int? page)
        {
            int _page = (int)(page ?? 1);

            EventsListViewModel _eventsList = new EventsListViewModel
            {
                EventsItems = _repo.GetEvents().Skip((_page - 1) * _eventsPageSize).Take(_eventsPageSize),
                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _eventsPageSize,
                    TotalItems = _repo.GetEvents().Count()
                }
            };


            return View(_eventsList);
        }

        public IActionResult TicketsEvent(int? id, int? page)
        {
            EventTicketsListView _eventsList;

            if (id != null)
            {
                int _id = (int)id;

                Event _event = _repo.GetEventById(_id);

                if (_event != null)
                {
                    int _page = (int)(page ?? 1);
                    _eventsList = new EventTicketsListView
                    {
                        Event = _event,


                        PagingInfo = new PagingInfo
                        {
                            CurentPage = _page,
                            ItemsRerPage = _ticketsForEventsPageSize,
                            TotalItems = _event.Tickets.Count()
                        }
                    };
                    return View(_eventsList);
                }
            }
            return RedirectToAction("Events");
        }


        [HttpGet]
        [Authorize]
        public IActionResult SellTickets(int? id)
        {


            if (id != null)
            {
                int _id = (int)id;

                Event _event = _repo.GetEventById(_id);

                if (_event != null)
                {

                    return View(_event);
                }
            }
            return RedirectToAction("Events");
        }

        [HttpPost]
        [Authorize]
        public IActionResult SellTickets(Ticket ticket)
        {
            if (ticket.EventId > 0)
            {
                User _curUser = _repo.GetUserByEmail(HttpContext.User.Identity.Name);

                if (_curUser != null)
                {
                    ticket.UserId = _curUser.Id;
                    ticket.TicketStatusId = _repo.GetTicketStatusByName(TicketStatusEnum.SellingTicket.ToString("g")).Id;
                    _repo.AddTicket(ticket);
                }
            }

            return RedirectToAction("TicketsEvent", new { id = ticket.EventId });
        }


        public IActionResult MakeOrder(int? id, string returnUrl)
        {
            if (id != null)
            {
                int _id = (int)id;

                User _curUser = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
                Order _order = new Order { TicketId = id, UserId = _curUser.Id, OrderStatusId = _repo.GetOrderStatusByName(OrderStatusEnum.WaitingOrder.ToString("g")).Id };
                _repo.AddOrder(_order);
                _repo.SetTicketStatus(_id, TicketStatusEnum.WaitingTicket);

                return LocalRedirect(returnUrl);

            }

            return LocalRedirect("Error");
        }

        public IActionResult ConfirmOrder(int? ticketId, string returnUrl)
        {
            if (ticketId != null)
            {
                int _ticketId = (int)ticketId;

                Order _order = _repo.GetOrderByTicketId(_ticketId);

                if (_order != null)
                {
                    _repo.SetOrderStatus(_order.Id, OrderStatusEnum.ConfirmedOrder);

                    _repo.SetTicketStatus(_ticketId, TicketStatusEnum.SoldTicket);

                    _repo.AddTrackingNumberToOrder(_order.Id, new Random().Next(1, 10000));

                    return LocalRedirect(returnUrl);
                }
            }
            return LocalRedirect("Error");
        }

        public IActionResult RejectOrder(int? ticketId, string returnUrl)
        {
            if (ticketId != null)
            {
                int _ticketId = (int)ticketId;

                Order _order = _repo.GetOrderByTicketId(_ticketId);

                if (_order != null)
                {
                    _repo.SetOrderStatus(_order.Id, OrderStatusEnum.RejectedOrder);

                    _repo.SetTicketStatus(_ticketId, TicketStatusEnum.SellingTicket);

                    _repo.AddErrorMesageToOrder(_order.Id, "Sorry man!");

                    return LocalRedirect(returnUrl);
                }
            }
            return LocalRedirect("Error");
        }



        public IActionResult SetLanguage(string culture, string returnUrl)
        {

            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );


            return LocalRedirect(returnUrl);

            // return View(System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName);culture
        }
        public IActionResult Error()
        {
            return View("Error");
        }

    }
}
