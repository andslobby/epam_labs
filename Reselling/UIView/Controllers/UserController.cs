﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using Reselling.Domain.Models.Authentication;
using Microsoft.AspNetCore.Authorization;

namespace Resaling.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        
        IRepository _repo;

        public UserController(IRepository repo)
        {
            _repo = repo;
        }


        public IActionResult MyTickets()
        {

            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                UserTicketsViewModel _userTickets = new UserTicketsViewModel
                {
                    User = _user,
                    SellingTikets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == _repo.GetTicketStatusByName(TicketStatusEnum.SellingTicket.ToString("g")).Id),
                    WaitingTickets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == _repo.GetTicketStatusByName(TicketStatusEnum.WaitingTicket.ToString("g")).Id),
                    SoldTickets = _user.Tickets.Where<Ticket>(t => t.TicketStatusId == _repo.GetTicketStatusByName(TicketStatusEnum.SoldTicket.ToString("g")).Id),

                };
                return View(_userTickets);
            };
            return RedirectToAction("Events", "Main");
        }


        public IActionResult MyOrders()
        {
            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                return View(_user);
            };
            return RedirectToAction("Events", "Main");
        }

        [HttpGet]
        public IActionResult MyAccount()
        { 
            User _user = _repo.GetUserByEmail(HttpContext.User.Identity.Name);
            if (_user != null)
            {
                return View(_user);
            };
            return RedirectToAction("Events", "Main");
        }

        [HttpPost]
        public IActionResult MyAccount(User user)
        {
            if (ModelState.IsValid)
            {
                 _repo.UpdateUser(user);
            };
            return RedirectToAction("Events", "Main");
        }



    }
}
