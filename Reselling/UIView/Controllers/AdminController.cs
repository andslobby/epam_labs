﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Net.Http.Headers;
using Reselling.Domain.Infrastructure;
using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UIView.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        IRepository _repo;
        readonly int _citiesPageSize = 10;
        readonly int _venuesPageSize = 10;
        readonly int _eventsPageSize = 10;
        readonly int _usersPageSize = 10;


        public AdminController(IRepository repo)
        {
            _repo = repo;
        }

        #region //Manage Cities

        public IActionResult Cities(int? page)
        {
            int _page = (int)(page ?? 1);

            CitiesListViewModel _citiesList = new CitiesListViewModel
            {
                CitiesItems = _repo.GetCities().Skip((_page - 1) * _citiesPageSize).Take(_citiesPageSize),
                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _citiesPageSize,
                    TotalItems = _repo.GetCities().Count()
                }
            };


            return View(_citiesList);
        }

        [HttpGet]
        public IActionResult UpdateCity(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                City _city = _repo.GetCityById(_id);

                return View(_city);
            }

            return RedirectToAction("Error", "Main");

        }

        [HttpPost]
        public IActionResult UpdateCity(City city)
        {
            if (ModelState.IsValid)
            {
                _repo.UpdateCity(city);
            }
            else
            {
                return View(city);
            };
            return RedirectToAction("Cities");
        }

        [HttpGet]
        public IActionResult CreateCity()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateCity(City city)
        {
            if (ModelState.IsValid)
            {
                _repo.AddCity(city);
            }
            else
            {
                return View(city);
            };
            return RedirectToAction("Cities");
        }

        [HttpGet]
        public IActionResult DeleteCity(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                City _city = _repo.GetCityById(_id);

                return View(_city);
            }

            return RedirectToAction("Error", "Main");
        }


        [HttpPost]
        [ActionName("DeleteCity")]
        public IActionResult ConfirmeDeleteCity(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                _repo.DeleteCityById(_id);

                return RedirectToAction("Cities");
            }

            return RedirectToAction("Error", "Main");

        }

        #endregion

        #region //Manage Venues

        public IActionResult Venues(int? page)
        {
            int _page = (int)(page ?? 1);

            VenuesListViewModel _venuesList = new VenuesListViewModel
            {
                VenuesItems = _repo.GetVenues().Skip((_page - 1) * _venuesPageSize).Take(_venuesPageSize),
                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _venuesPageSize,
                    TotalItems = _repo.GetVenues().Count()
                }
            };


            return View(_venuesList);
        }

        [HttpGet]
        public IActionResult UpdateVenue(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                Venue _venue = _repo.GetVenueById(_id);
                SelectList cities = new SelectList(_repo.GetCities(), "Id", "Name");
                ViewBag.Cities = cities;

                return View(_venue);
            }

            return RedirectToAction("Error", "Main");

        }

        [HttpPost]
        public IActionResult UpdateVenue(Venue venue)
        {
            if (ModelState.IsValid)
            {
                _repo.UpdateVenue(venue);
            };
            return RedirectToAction("Venues");
        }

        [HttpGet]
        public IActionResult CreateVenue()
        {
            SelectList cities = new SelectList(_repo.GetCities(), "Id", "Name");
            ViewBag.Cities = cities;

            return View();
        }

        [HttpPost]
        public IActionResult CreateVenue(Venue venue)
        {
            if (ModelState.IsValid)
            {
                _repo.AddVenue(venue);
            };
            return RedirectToAction("Venues");
        }

        [HttpGet]
        public IActionResult DeleteVenue(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                Venue _venue = _repo.GetVenueById(_id);

                return View(_venue);
            }

            return RedirectToAction("Error", "Main");
        }


        [HttpPost]
        [ActionName("DeleteVenue")]
        public IActionResult ConfirmeDeleteVenue(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                _repo.DeleteVenueById(_id);

                return RedirectToAction("Venues");
            }

            return RedirectToAction("Error", "Main");

        }

        #endregion

        #region //Manage Events

        public IActionResult Events(int? page)
        {
            int _page = (int)(page ?? 1);

            EventsListViewModel _citiesList = new EventsListViewModel
            {
                EventsItems = _repo.GetEvents().Skip((_page - 1) * _eventsPageSize).Take(_eventsPageSize),
                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _eventsPageSize,
                    TotalItems = _repo.GetEvents().Count()
                }
            };


            return View(_citiesList);
        }

        [HttpGet]
        public IActionResult UpdateEvent(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;
                Event _ev = _repo.GetEventById(_id);
                IEnumerable<SelectListItem> venues = _repo.GetVenues().Select(v => new SelectListItem { Text = $@"{v.City.Name} - {v.Name}", Value = v.Id.ToString() });
                ViewBag.Venues = venues;
                return View(_ev);
            }
            return RedirectToAction("Error", "Main");
        }

        [HttpPost]
        public IActionResult UpdateEvent(Event ev, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.Length > 0)
                {
                    ev.Banner = $@"Movies/{ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"')}";
                    _repo.SaveBanner(file);

                }
                //  ev.Banner = $@"Movies/{ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"')}";
                _repo.UpdateEvent(ev);
            };
            return RedirectToAction("Events");
        }

        [HttpGet]
        public IActionResult CreateEvent()
        {
            IEnumerable<SelectListItem> venues = _repo.GetVenues().Select(v => new SelectListItem { Text = $@"{v.City.Name} - {v.Name}", Value = v.Id.ToString() });
            ViewBag.Venues = venues;
            return View();
        }

        [HttpPost]
        public IActionResult CreateEvent(Event ev, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.Length > 0)
                {
                    _repo.SaveBanner(file);
                    ev.Banner = $@"Movies/{ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"')}";
                }
                _repo.AddEvent(ev);
            }
            return RedirectToAction("Events");
        }

        [HttpGet]
        public IActionResult DeleteEvent(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;
                Event _ev = _repo.GetEventById(_id);
                return View(_ev);
            }
            return RedirectToAction("Error", "Main");
        }

        [HttpPost]
        [ActionName("DeleteEvent")]
        public IActionResult ConfirmeDeleteEvent(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;
                _repo.DeleteEventById(_id);
                return RedirectToAction("Events");
            }
            return RedirectToAction("Error", "Main");
        }
        #endregion

        #region //Manage Users
        public IActionResult Users(int? page)
        {
            int _page = (int)(page ?? 1);

            UsersListViewModel _usersList = new UsersListViewModel
            {

                UsersItem = _repo.GetUsers().Skip((_page - 1) * _usersPageSize).Take(_usersPageSize),

                PagingInfo = new PagingInfo
                {
                    CurentPage = _page,
                    ItemsRerPage = _usersPageSize,
                    TotalItems = _repo.GetUsers().Count()
                }
            };

            return View(_usersList);
        }

        [HttpGet]
        public IActionResult ChangeUserRole(int? id)
        {
            if (id != null)
            {
                int _id = (int)id;

                User _user = _repo.GetUserById(_id);
                IEnumerable<SelectListItem> roles = new SelectList(_repo.GetRoles(), "Id", "Name");
                ViewBag.Roles = roles;
                return View(_user);
            }
            return RedirectToAction("Error", "Main");
        }


        [HttpPost]
        public IActionResult ChangeUserRole(User user)
        {

            if (ModelState.IsValid)
            {
                _repo.UpdateUserRole(user);
            };
            return RedirectToAction("Users", "Admin");

        }

        #endregion
    }

}

