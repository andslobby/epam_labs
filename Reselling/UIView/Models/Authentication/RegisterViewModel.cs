﻿using System.ComponentModel.DataAnnotations;

namespace Reselling.UIView.Models.Authentication
{
    public class RegisterViewModel
    {

        [Required(ErrorMessage = "Set Email")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address")]
        [Display(Name = "Enter Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Set password")]
        [DataType(DataType.Password)]
        [Display(Name = "Enter Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Difrent passwords")]
        [Display(Name = "Repit Password")]
        public string ConfirmPassword { get; set; }




    }


}
