﻿using System.ComponentModel.DataAnnotations;


namespace Reselling.UIView.Models.Authentication
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Set Email")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address")]
        [Display(Name = "Enter Email")]
        public string Email { get; set; }
               
        [Required(ErrorMessage = "Set Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Enter password")]
        public string Password { get; set; }
    }
}
