﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reselling.Domain.Models.Data;

namespace Reselling.UIView.Models.Data
{
    public class VenuesListViewModel
    {
        public IEnumerable<Venue> VenuesItems { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}
