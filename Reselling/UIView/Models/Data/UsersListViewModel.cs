﻿using System.Collections.Generic;
using Reselling.Domain.Models.Authentication;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Reselling.UIView.Models.Data
{
    public class UsersListViewModel
    {
        public IEnumerable<User> UsersItem {get; set; }

        public SelectList Roles { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}