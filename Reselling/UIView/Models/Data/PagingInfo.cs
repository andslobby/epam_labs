﻿using System;

namespace Reselling.UIView.Models.Data
{
    public class PagingInfo
    {
        public int TotalItems { get; set; }

        public int ItemsRerPage { get; set; }

        public int CurentPage { get; set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsRerPage); }
        }
    }
}
