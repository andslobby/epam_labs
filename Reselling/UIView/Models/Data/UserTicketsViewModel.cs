﻿using Reselling.Domain.Models.Data;
using System.Collections.Generic;
using Reselling.Domain.Models.Authentication;


namespace Reselling.UIView.Models.Data
{
    public class UserTicketsViewModel
    {
        public User User;

        public IEnumerable<Ticket> SellingTikets;
        public IEnumerable<Ticket> WaitingTickets;
        public IEnumerable<Ticket> SoldTickets;
    }
}
