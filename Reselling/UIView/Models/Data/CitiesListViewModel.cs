﻿using Reselling.Domain.Models.Data;
using Reselling.UIView.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.UIView.Models.Data
{
    public class CitiesListViewModel
    {
        public IEnumerable<City> CitiesItems { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}
