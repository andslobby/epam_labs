﻿using System.Collections.Generic;
using Reselling.Domain.Models.Data;
using Microsoft.AspNetCore.Http;

namespace Reselling.UIView.Models.Data
{
    public class EventViewModel
    {
        public Event Event { get; set; }

        public IFormFile FileBanner { get; set; }
    }
}
