﻿using System.Collections.Generic;
using Reselling.Domain.Models.Data;

namespace Reselling.UIView.Models.Data
{
    public class EventsListViewModel
    {
        public IEnumerable<Event> EventsItems { get; set; }

        public PagingInfo PagingInfo { get; set; }
    }
}
