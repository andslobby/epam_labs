﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Data
{
    public class Event
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public int VenueId { get; set; }

        public Venue Venue { get; set; }

        public string Banner { get; set; }

        public string Description { get; set; }

        public List<Ticket> Tickets { get; set; }

    }
}