﻿using Reselling.Domain.Models.Authentication;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Data
{
    public class Order
    {
        [Key]
        public int Id { get; set; }

        public int? TicketId { get; set; }

        public Ticket Ticket { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int OrderStatusId { get; set; }

        public OrderStatus OrderStatus { get; set; }

        public int TrackingNumber { get; set; }

        public string RejectingMessage { get; set; }


    }
}