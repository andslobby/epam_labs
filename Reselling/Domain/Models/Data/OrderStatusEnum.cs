﻿namespace Reselling.Domain.Models.Data
{
    public enum OrderStatusEnum
    {
        WaitingOrder = 1,
        ConfirmedOrder,
        RejectedOrder
    }
}
