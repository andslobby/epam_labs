﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Reselling.Domain.Models.Authentication;

namespace Reselling.Domain.Models.Data
{
    public class DataContext : DbContext
    {

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<City> Cities { get; set; }
        
        public DbSet<Venue> Venues { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Ticket> Tickets { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<TicketStatus> TicketStatuses { get; set; }
        
        public DbSet<OrderStatus> OrderStatuses { get; set; }


        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        { }
               
    }
}
