﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Data
{
    public class OrderStatus
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Order> Orders { get; set; }
    }
}
