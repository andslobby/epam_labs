﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Data
{
    public class TicketStatus
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Ticket> Tickets { get; set; }
    }
}

