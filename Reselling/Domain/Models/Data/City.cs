﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Data
{
    public class City
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Venue> Venues { get; set; }

    }
}