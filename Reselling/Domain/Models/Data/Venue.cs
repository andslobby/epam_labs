﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Reselling.Domain.Models.Data
{
    public class Venue
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Adress { get; set; }

        public int CityId { get; set; }

        public City City { get; set; }

        public List<Event> Events { get; set; }

        
    }
}