﻿using System.Collections.Generic;
using Reselling.Domain.Models.Data;
using System.ComponentModel.DataAnnotations;

namespace Reselling.Domain.Models.Authentication
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Country { get; set; }

        public string Adress { get; set; }

        public string PhoneNumber { get; set; }

        public string Password { get; set; }

        public int RoleId { get; set; }

        public Role Role { get; set; }

        public List<Ticket> Tickets { get; set; }

        public List<Order> Orders { get; set; }
    }
}