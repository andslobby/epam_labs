﻿using System.Security.Cryptography;
using System.Text;

namespace Reselling.Domain.Infrastructure
{
    public class Security
    {
        public static string Hash(string pw)
        {
            byte[] _bytes = Encoding.Unicode.GetBytes(pw);

            using (var _md5 = MD5.Create())
            {
                var result = _md5.ComputeHash(_bytes);
                return Encoding.Unicode.GetString(result);
            }
        }
    }
}
