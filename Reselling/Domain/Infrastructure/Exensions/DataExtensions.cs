﻿using System;
using System.Collections.Generic;
using System.Text;
using Reselling.Domain.Models.Data;
using Microsoft.Extensions.Configuration;
using System.Linq;
using Reselling.Domain.Models.Authentication;
using System.Globalization;

namespace Reselling.Domain.Infrastructure.Exensions
{
    public static class DataExtensions
    {
        public static void ResellingSeedData(this DataContext context, IConfigurationSection section)
        {
            CultureInfo ci = new CultureInfo("en-US");

            if (context.AllMigrationsApplied())
            {
                if (!context.Roles.Any())
                {
                    #region //define roles

                    Role _user = context.Roles.Add(new Role { Name = "User" }).Entity;
                    Role _admin = context.Roles.Add(new Role { Name = "Admin" }).Entity;

                    context.SaveChanges();
                    #endregion

                    if (!context.Users.Any())
                    {
                        #region //define users

                        User _user1 = context.Users.Add(new User { FirstName = "User1", LastName = "User1", RoleId = _user.Id, Email = "1@yandex.ru", Password = Security.Hash("user") }).Entity;
                        User _user2 = context.Users.Add(new User { FirstName = "User2", LastName = "User2", RoleId = _user.Id, Email = "2@yandex.ru", Password = Security.Hash("user") }).Entity;
                        User _user3 = context.Users.Add(new User { FirstName = "admin", LastName = "admin", RoleId = _admin.Id, Email = "3@yandex.ru", Password = Security.Hash("admin") }).Entity;
                        User _user4 = context.Users.Add(new User { FirstName = "User4", LastName = "User4", RoleId = _user.Id, Email = "4@yandex.ru", Password = Security.Hash("user") }).Entity;

                        context.SaveChanges();
                        #endregion


                        if (!context.Cities.Any())
                        {
                            #region //define cities

                            City _city1 = context.Cities.Add(new City { Name = "Brest" }).Entity;
                            City _city2 = context.Cities.Add(new City { Name = "Gomel" }).Entity;
                            City _city3 = context.Cities.Add(new City { Name = "Grodno" }).Entity;
                            City _city4 = context.Cities.Add(new City { Name = "Vitebsk" }).Entity;
                            City _city5 = context.Cities.Add(new City { Name = "Minsk" }).Entity;
                            City _city6 = context.Cities.Add(new City { Name = "Mogilev" }).Entity;

                            context.SaveChanges();
                            #endregion


                            if (!context.Venues.Any())
                            {
                                #region //define venues
                                Venue _venue1 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city1.Id }).Entity;
                                Venue _venue2 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city1.Id }).Entity;
                                Venue _venue3 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city2.Id }).Entity;
                                Venue _venue4 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city2.Id }).Entity;
                                Venue _venue5 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city3.Id }).Entity;
                                Venue _venue6 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city3.Id }).Entity;
                                Venue _venue7 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city4.Id }).Entity;
                                Venue _venue8 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city4.Id }).Entity;
                                Venue _venue9 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city5.Id }).Entity;
                                Venue _venue10 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city5.Id }).Entity;
                                Venue _venue11 = context.Venues.Add(new Venue { Name = "Palace", Adress = "str", CityId = _city6.Id }).Entity;
                                Venue _venue12 = context.Venues.Add(new Venue { Name = "Sdadium", Adress = "str", CityId = _city6.Id }).Entity;
                                Venue _venue13 = context.Venues.Add(new Venue { Name = "Beach", Adress = "str", CityId = _city6.Id }).Entity;

                                context.SaveChanges();

                                #endregion

                                if (!context.Events.Any())
                                {

                                    #region //define events

                                    Event _event1 = context.Events.Add(new Event
                                    {
                                        Name = "Snatch",
                                        Date = DateTime.Parse("12.12.2016", ci),
                                        Banner = "Movies/Snatch.jpg",
                                        VenueId = _venue1.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event2 = context.Events.Add(new Event
                                    {
                                        Name = "Lock stock and two smoking barrels",
                                        Date = DateTime.Parse("12.13.2016", ci),
                                        Banner = "Movies/LockStock.jpg",
                                        VenueId = _venue2.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event3 = context.Events.Add(new Event
                                    {
                                        Name = "Fantastic Beasts and Where to Find Them",
                                        Date = DateTime.Parse("12.15.2016", ci),
                                        Banner = "Movies/Fantastic.jpg",
                                        VenueId = _venue3.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event4 = context.Events.Add(new Event
                                    {
                                        Name = "Arrival",
                                        Date = DateTime.Parse("12.19.2016", ci),
                                        Banner = "Movies/Arrival.jpg",
                                        VenueId = _venue4.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;


                                    Event _event5 = context.Events.Add(new Event
                                    {
                                        Name = "Masterminds",
                                        Date = DateTime.Parse("12.25.2016", ci),
                                        Banner = "Movies/Masterminds.jpg",
                                        VenueId = _venue5.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;


                                    Event _event6 = context.Events.Add(new Event
                                    {
                                        Name = "Magnificent Seven",
                                        Date = DateTime.Parse("12.24.2016", ci),
                                        Banner = "Movies/Seven.jpg",
                                        VenueId = _venue6.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;


                                    Event _event7 = context.Events.Add(new Event
                                    {
                                        Name = "Secret Life of Pets",
                                        Date = DateTime.Parse("12.29.2016", ci),
                                        Banner = "Movies/Pets.jpg",
                                        VenueId = _venue7.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event8 = context.Events.Add(new Event
                                    {
                                        Name = "Suicide Squad",
                                        Date = DateTime.Parse("12.31.2016", ci),
                                        Banner = "Movies/Squad.jpg",
                                        VenueId = _venue8.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event9 = context.Events.Add(new Event
                                    {
                                        Name = "Rogue One: A Star Wars Story",
                                        Date = DateTime.Parse("1.1.2017", ci),
                                        Banner = "Movies/StarWars.jpg",
                                        VenueId = _venue9.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event10 = context.Events.Add(new Event
                                    {
                                        Name = "Magnificent Seven",
                                        Date = DateTime.Parse("12.18.2016", ci),
                                        Banner = "Movies/Seven.jpg",
                                        VenueId = _venue10.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event11 = context.Events.Add(new Event
                                    {
                                        Name = "Snatch",
                                        Date = DateTime.Parse("12.17.2016", ci),
                                        Banner = "Movies/Snatch.jpg",
                                        VenueId = _venue11.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event12 = context.Events.Add(new Event
                                    {
                                        Name = "American Pastoral",
                                        Date = DateTime.Parse("2.2.2017", ci),
                                        Banner = "Movies/Pastoral.jpg",
                                        VenueId = _venue12.Id,
                                        Description = "Bla-bla-bla",
                                    }).Entity;

                                    Event _event13 = context.Events.Add(new Event
                                    {
                                        Name = "Arrival",
                                        Date = DateTime.Parse("3.3.2017", ci),
                                        Banner = "Movies/Arrival.jpg",
                                        VenueId = _venue13.Id,
                                        Description = "Bla-bla-bla",
                                        Tickets = null
                                    }).Entity;

                                    context.SaveChanges();
                                    #endregion

                                    if (!context.OrderStatuses.Any())
                                    {
                                        #region //define orderStatus
                                        OrderStatus _orderStatus1 = context.OrderStatuses.Add(new OrderStatus { Name = OrderStatusEnum.WaitingOrder.ToString("g") }).Entity;
                                        OrderStatus _orderStatus2 = context.OrderStatuses.Add(new OrderStatus { Name = OrderStatusEnum.ConfirmedOrder.ToString("g") }).Entity;
                                        OrderStatus _orderStatus3 = context.OrderStatuses.Add(new OrderStatus { Name = OrderStatusEnum.RejectedOrder.ToString("g") }).Entity;

                                        context.SaveChanges();

                                        #endregion


                                        if (!context.TicketStatuses.Any())
                                        {
                                            #region //define ticketStatus
                                            TicketStatus _ticketStatus1 = context.TicketStatuses.Add(new TicketStatus { Name = TicketStatusEnum.SellingTicket.ToString("g") }).Entity;
                                            TicketStatus _ticketStatus2 = context.TicketStatuses.Add(new TicketStatus { Name = TicketStatusEnum.WaitingTicket.ToString("g") }).Entity;
                                            TicketStatus _ticketStatus3 = context.TicketStatuses.Add(new TicketStatus { Name = TicketStatusEnum.SoldTicket.ToString("g") }).Entity;
                                            context.SaveChanges();
                                            #endregion
                                        }

                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}
