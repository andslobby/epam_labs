﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using System.Security.Cryptography;
using System.Text;
using System.Globalization;

namespace Reselling.Domain.Infrastructure
{
    //public class MemoryRepository //: IRepository
    //{
    //    private static int _id = 1;
    //    List<Event> _events;
    //    List<Ticket> _tickets;
    //    List<Order> _orders;
    //    List<User> _users;
    //    List<OrderStatus> _orderStatuses;
    //    List<TicketStatus> _ticketStatuses;

    //    public MemoryRepository()
    //    {

    //        CultureInfo ci = new CultureInfo("en-US");

    //        #region //define roles
    //        Role _role1 = new Role
    //        {
    //            Id = 1,
    //            Name = "User",
    //            Users = null
    //        };

    //        Role _role2 = new Role
    //        {
    //            Id = 2,
    //            Name = "Admin",
    //            Users = null
    //        };
    //        #endregion

    //        #region //define users

    //        User _user1 = new User
    //        {
    //            Id = _id++,
    //            FirstName = "user1",
    //            LastName = "user1",
    //            Password = Hash("user"),
    //            RoleId = 1,
    //            Role = _role1,
    //            Orders = null,
    //            Tickets = null,
    //            PhoneNumber = "1",
    //            Country = "By",
    //            Adress = "street1",
    //            Email = "1@yandex.ru",
    //        };

    //        User _user2 = new User
    //        {
    //            Id = _id++,
    //            FirstName = "user2",
    //            LastName = "user2",
    //            Password = Hash("user"),
    //            RoleId = 1,
    //            Role = _role1,
    //            Orders = null,
    //            Tickets = null,
    //            PhoneNumber = "1",
    //            Country = "By",
    //            Adress = "street1",
    //            Email = "2@yandex.ru",
    //        };

    //        User _user3 = new User
    //        {
    //            Id = _id++,
    //            FirstName = "admin",
    //            LastName = "admin",
    //            Password = Hash("admin"),
    //            RoleId = 2,
    //            Role = _role2,
    //            Orders = null,
    //            Tickets = null,
    //            PhoneNumber = "1",
    //            Country = "By",
    //            Adress = "street1",
    //            Email = "3@yandex.ru",
    //        };

    //        User _user4 = new User
    //        {
    //            Id = _id++,
    //            FirstName = "user4",
    //            LastName = "user4",
    //            Password = Hash("user"),
    //            RoleId = 1,
    //            Role = _role1,
    //            Orders = null,
    //            Tickets = null,
    //            PhoneNumber = "1",
    //            Country = "By",
    //            Adress = "street1",
    //            Email = "4@yandex.ru",
    //        };
    //        #endregion

    //        #region //define cities
    //        City _city1 = new City { Id = 1, Name = "Brest", Venues = null };
    //        City _city2 = new City { Id = 2, Name = "Gomel", Venues = null };
    //        City _city3 = new City { Id = 3, Name = "Grodno", Venues = null };
    //        City _city4 = new City { Id = 4, Name = "Vitebsk", Venues = null };
    //        City _city5 = new City { Id = 5, Name = "Minsk", Venues = null };
    //        City _city6 = new City { Id = 6, Name = "Mogilev", Venues = null };

    //        #endregion

    //        #region //define venues
    //        Venue _venue1 = new Venue { Id = 1, Name = "Palace", Adress = "str", City = _city1, CityId = 1, Events = null };
    //        Venue _venue2 = new Venue { Id = 2, Name = "Sdadium", Adress = "str", City = _city1, CityId = 1, Events = null };
    //        Venue _venue3 = new Venue { Id = 3, Name = "Palace", Adress = "str", City = _city2, CityId = 2, Events = null };
    //        Venue _venue4 = new Venue { Id = 1, Name = "Sdadium", Adress = "str", City = _city2, CityId = 2, Events = null };
    //        Venue _venue5 = new Venue { Id = 1, Name = "Palace", Adress = "str", City = _city3, CityId = 3, Events = null };
    //        Venue _venue6 = new Venue { Id = 1, Name = "Sdadium", Adress = "str", City = _city3, CityId = 3, Events = null };
    //        Venue _venue7 = new Venue { Id = 1, Name = "Palace", Adress = "str", City = _city4, CityId = 4, Events = null };
    //        Venue _venue8 = new Venue { Id = 1, Name = "Sdadium", Adress = "str", City = _city4, CityId = 4, Events = null };
    //        Venue _venue9 = new Venue { Id = 1, Name = "Palace", Adress = "str", City = _city5, CityId = 5, Events = null };
    //        Venue _venue10 = new Venue { Id = 1, Name = "Sdadium", Adress = "str", City = _city5, CityId = 5, Events = null };
    //        Venue _venue11 = new Venue { Id = 1, Name = "Palace", Adress = "str", City = _city6, CityId = 6, Events = null };
    //        Venue _venue12 = new Venue { Id = 1, Name = "Sdadium", Adress = "str", City = _city6, CityId = 6, Events = null };
    //        Venue _venue13 = new Venue { Id = 1, Name = "Beach", Adress = "str", City = _city6, CityId = 6, Events = null };


    //        #endregion

    //        #region //define events

    //        Event _event1 = new Event
    //        {
    //            Id = 1,
    //            Name = "Snatch",
    //            Date = DateTime.Parse("12.12.2016", ci),
    //            Banner = "Movies/Snatch.jpg",
    //            VenueId = 1,
    //            Venue = _venue1,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event2 = new Event
    //        {
    //            Id = 2,
    //            Name = "Lock stock and two smoking barrels",
    //            Date = DateTime.Parse("12.13.2016", ci),
    //            Banner = "Movies/LockStock.jpg",
    //            VenueId = 2,
    //            Venue = _venue2,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event3 = new Event
    //        {
    //            Id = 3,
    //            Name = "Fantastic Beasts and Where to Find Them",
    //            Date = DateTime.Parse("12.15.2016", ci),
    //            Banner = "Movies/Fantastic.jpg",
    //            VenueId = 3,
    //            Venue = _venue3,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event4 = new Event
    //        {
    //            Id = 4,
    //            Name = "Arrival",
    //            Date = DateTime.Parse("12.19.2016", ci),
    //            Banner = "Movies/Arrival.jpg",
    //            VenueId = 4,
    //            Venue = _venue4,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };


    //        Event _event5 = new Event
    //        {
    //            Id = 5,
    //            Name = "Masterminds",
    //            Date = DateTime.Parse("12.25.2016", ci),
    //            Banner = "Movies/Masterminds.jpg",
    //            VenueId = 5,
    //            Venue = _venue5,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };


    //        Event _event6 = new Event
    //        {
    //            Id = 6,
    //            Name = "Magnificent Seven",
    //            Date = DateTime.Parse("12.24.2016", ci),
    //            Banner = "Movies/Seven.jpg",
    //            VenueId = 6,
    //            Venue = _venue6,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };


    //        Event _event7 = new Event
    //        {
    //            Id = 7,
    //            Name = "Secret Life of Pets",
    //            Date = DateTime.Parse("12.29.2016", ci),
    //            Banner = "Movies/Pets.jpg",
    //            VenueId = 7,
    //            Venue = _venue7,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event8 = new Event
    //        {
    //            Id = 8,
    //            Name = "Suicide Squad",
    //            Date = DateTime.Parse("12.31.2016", ci),
    //            Banner = "Movies/Squad.jpg",
    //            VenueId = 8,
    //            Venue = _venue8,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event9 = new Event
    //        {
    //            Id = 9,
    //            Name = "Rogue One: A Star Wars Story",
    //            Date = DateTime.Parse("1.1.2017", ci),
    //            Banner = "Movies/StarWars.jpg",
    //            VenueId = 9,
    //            Venue = _venue9,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event10 = new Event
    //        {
    //            Id = 10,
    //            Name = "Magnificent Seven",
    //            Date = DateTime.Parse("12.18.2016", ci),
    //            Banner = "Movies/Seven.jpg",
    //            VenueId = 10,
    //            Venue = _venue10,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event11 = new Event
    //        {
    //            Id = 11,
    //            Name = "Snatch",
    //            Date = DateTime.Parse("12.17.2016", ci),
    //            Banner = "Movies/Snatch.jpg",
    //            VenueId = 11,
    //            Venue = _venue11,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event12 = new Event
    //        {
    //            Id = 12,
    //            Name = "American Pastoral",
    //            Date = DateTime.Parse("2.2.2017", ci),
    //            Banner = "Movies/Pastoral.jpg",
    //            VenueId = 12,
    //            Venue = _venue12,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        Event _event13 = new Event
    //        {
    //            Id = 13,
    //            Name = "Arrival",
    //            Date = DateTime.Parse("3.3.2017", ci),
    //            Banner = "Movies/Arrival.jpg",
    //            VenueId = 13,
    //            Venue = _venue13,
    //            Description = "Bla-bla-bla",
    //            Tickets = null
    //        };

    //        #endregion

    //        #region //define orderStatus
    //        OrderStatus _orderStatus1 = new OrderStatus { Id = (int)OrderStatusEnum.WaitingOrder, Name = OrderStatusEnum.WaitingOrder.ToString(), Orders = null };
    //        OrderStatus _orderStatus2 = new OrderStatus { Id = (int)OrderStatusEnum.ConfirmedOrder, Name = OrderStatusEnum.ConfirmedOrder.ToString(), Orders = null };
    //        OrderStatus _orderStatus3 = new OrderStatus { Id = (int)OrderStatusEnum.RejectedOrder, Name = OrderStatusEnum.RejectedOrder.ToString(), Orders = null };

    //        #endregion

    //        #region //define ticketStatus
    //        TicketStatus _ticketStatus1 = new TicketStatus { Id = (int)TicketStatusEnum.SellingTicket, Name = TicketStatusEnum.SellingTicket.ToString(), Tickets = null };
    //        TicketStatus _ticketStatus2 = new TicketStatus { Id = (int)TicketStatusEnum.WaitingTicket, Name = TicketStatusEnum.WaitingTicket.ToString(), Tickets = null };
    //        TicketStatus _ticketStatus3 = new TicketStatus { Id = (int)TicketStatusEnum.SoldTicket, Name = TicketStatusEnum.SoldTicket.ToString(), Tickets = null };

    //        #endregion

    //        #region //define tickets
    //        Ticket _ticket1 = new Ticket { Id = 1, EventId = 1, Event = _event1, Price = 26, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket2 = new Ticket { Id = 2, EventId = 1, Event = _event1, Price = 25, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket3 = new Ticket { Id = 3, EventId = 2, Event = _event2, Price = 65, User = _user3, UserId = 3, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket4 = new Ticket { Id = 4, EventId = 2, Event = _event2, Price = 2, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 2, TicketStatus = _ticketStatus2 };
    //        Ticket _ticket5 = new Ticket { Id = 5, EventId = 5, Event = _event5, Price = 75, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket6 = new Ticket { Id = 6, EventId = 6, Event = _event6, Price = 65, User = _user4, UserId = 4, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket7 = new Ticket { Id = 7, EventId = 13, Event = _event13, Price = 12, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket8 = new Ticket { Id = 8, EventId = 4, Event = _event4, Price = 36, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket9 = new Ticket { Id = 9, EventId = 12, Event = _event12, Price = 55, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket10 = new Ticket { Id = 10, EventId = 6, Event = _event6, Price = 42, User = _user3, UserId = 3, Note = "bla-bla", TicketStatusId = 3, TicketStatus = _ticketStatus3 };
    //        Ticket _ticket11 = new Ticket { Id = 11, EventId = 7, Event = _event7, Price = 26, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket12 = new Ticket { Id = 12, EventId = 8, Event = _event8, Price = 25, User = _user4, UserId = 4, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket13 = new Ticket { Id = 13, EventId = 2, Event = _event2, Price = 65, User = _user3, UserId = 3, Note = "bla-bla", TicketStatusId = 2, TicketStatus = _ticketStatus2 };
    //        Ticket _ticket14 = new Ticket { Id = 14, EventId = 4, Event = _event4, Price = 2, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 2, TicketStatus = _ticketStatus2 };
    //        Ticket _ticket15 = new Ticket { Id = 15, EventId = 5, Event = _event5, Price = 75, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket16 = new Ticket { Id = 16, EventId = 6, Event = _event6, Price = 65, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket17 = new Ticket { Id = 17, EventId = 10, Event = _event10, Price = 12, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 3, TicketStatus = _ticketStatus3 };
    //        Ticket _ticket18 = new Ticket { Id = 18, EventId = 5, Event = _event5, Price = 36, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket19 = new Ticket { Id = 19, EventId = 12, Event = _event12, Price = 55, User = _user1, UserId = 1, Note = "bla-bla", TicketStatusId = 1, TicketStatus = _ticketStatus1 };
    //        Ticket _ticket20 = new Ticket { Id = 20, EventId = 6, Event = _event6, Price = 42, User = _user2, UserId = 2, Note = "bla-bla", TicketStatusId = 2, TicketStatus = _ticketStatus2 };
    //        #endregion

    //        #region //define orders
    //        Order _order1 = new Order { Id = 1, TicketId = 1, Ticket = _ticket1, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 1, User = _user1, TrackNumber = 20 };
    //        Order _order2 = new Order { Id = 2, TicketId = 2, Ticket = _ticket2, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 1, User = _user1, TrackNumber = 1 };
    //        Order _order3 = new Order { Id = 3, TicketId = 3, Ticket = _ticket3, OrderStatusId = 3, OrderStatus = _orderStatus3, UserId = 3, User = _user3, TrackNumber = 2 };
    //        Order _order4 = new Order { Id = 4, TicketId = 4, Ticket = _ticket4, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 1, User = _user1, TrackNumber = 3 };
    //        Order _order5 = new Order { Id = 5, TicketId = 5, Ticket = _ticket5, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 2, User = _user2, TrackNumber = 4 };
    //        Order _order6 = new Order { Id = 6, TicketId = 6, Ticket = _ticket6, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 1, User = _user1, TrackNumber = 5 };
    //        Order _order7 = new Order { Id = 7, TicketId = 8, Ticket = _ticket8, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 1, User = _user1, TrackNumber = 6 };
    //        Order _order8 = new Order { Id = 8, TicketId = 9, Ticket = _ticket9, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 2, User = _user2, TrackNumber = 7 };
    //        Order _order9 = new Order { Id = 9, TicketId = 10, Ticket = _ticket10, OrderStatusId = 3, OrderStatus = _orderStatus3, UserId = 1, User = _user1, TrackNumber = 8 };
    //        Order _order10 = new Order { Id = 10, TicketId = 7, Ticket = _ticket7, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 3, User = _user3, TrackNumber = 9 };
    //        Order _order11 = new Order { Id = 11, TicketId = 1, Ticket = _ticket1, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 1, User = _user1, TrackNumber = 10 };
    //        Order _order12 = new Order { Id = 12, TicketId = 2, Ticket = _ticket2, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 1, User = _user1, TrackNumber = 11 };
    //        Order _order13 = new Order { Id = 13, TicketId = 3, Ticket = _ticket3, OrderStatusId = 3, OrderStatus = _orderStatus3, UserId = 3, User = _user3, TrackNumber = 12 };
    //        Order _order14 = new Order { Id = 14, TicketId = 4, Ticket = _ticket4, OrderStatusId = 3, OrderStatus = _orderStatus3, UserId = 1, User = _user1, TrackNumber = 13 };
    //        Order _order15 = new Order { Id = 15, TicketId = 5, Ticket = _ticket5, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 2, User = _user2, TrackNumber = 14 };
    //        Order _order16 = new Order { Id = 16, TicketId = 6, Ticket = _ticket6, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 1, User = _user1, TrackNumber = 15 };
    //        Order _order17 = new Order { Id = 17, TicketId = 8, Ticket = _ticket8, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 1, User = _user1, TrackNumber = 16 };
    //        Order _order18 = new Order { Id = 18, TicketId = 9, Ticket = _ticket9, OrderStatusId = 2, OrderStatus = _orderStatus2, UserId = 2, User = _user2, TrackNumber = 17 };
    //        Order _order19 = new Order { Id = 19, TicketId = 10, Ticket = _ticket10, OrderStatusId = 3, OrderStatus = _orderStatus3, UserId = 1, User = _user1, TrackNumber = 18 };
    //        Order _order20 = new Order { Id = 20, TicketId = 7, Ticket = _ticket7, OrderStatusId = 1, OrderStatus = _orderStatus1, UserId = 3, User = _user3, TrackNumber = 19 };

    //        #endregion

    //        _events = new List<Event> { _event1, _event2, _event3, _event4, _event5, _event6, _event7, _event8, _event9, _event10, _event11, _event12, _event13 };
    //        _tickets = new List<Ticket> { _ticket1, _ticket2, _ticket3, _ticket4, _ticket5, _ticket6, _ticket7, _ticket8, _ticket9, _ticket10,
    //        _ticket11, _ticket12, _ticket13, _ticket14, _ticket15, _ticket16, _ticket17, _ticket18, _ticket19, _ticket20};
    //        _orders = new List<Order> { _order1, _order2, _order3, _order4, _order5, _order6, _order7, _order8, _order9, _order10 };
    //        _users = new List<User> { _user1, _user2, _user3, _user4 };
    //        _orderStatuses = new List<OrderStatus> { _orderStatus1, _orderStatus2, _orderStatus3 };
    //        _ticketStatuses = new List<TicketStatus> { _ticketStatus1, _ticketStatus2, _ticketStatus2 };
    //    }

    //    public Event GetEventById(int id)
    //    {
    //        Event _event = _events.FirstOrDefault<Event>(e => e.Id == id);
    //        if (_event != null)
    //        {
    //            _event.Tickets = _tickets.Where<Ticket>(t => t.EventId == id)
    //                .Where<Ticket>(t => t.TicketStatusId == _ticketStatuses.FirstOrDefault<TicketStatus>(os => os.Name == TicketStatusEnum.SellingTicket.ToString()).Id).ToList<Ticket>();
    //        }


    //        return _event;
    //    }

    //    public Ticket GetTicketById(int id)
    //    {
    //        return _tickets.FirstOrDefault<Ticket>(t => t.Id == id);
    //    }

    //    public IEnumerable<Event> GetEvents()
    //    {
    //        return _events;
    //    }

    //    public IEnumerable<Order> OrdersByUser(int id)
    //    {
    //        return _orders.Where<Order>(o => o.UserId == id); ;
    //    }

    //    public IEnumerable<Ticket> TicketsByUser(int id)
    //    {
    //        return _tickets.Where<Ticket>(t => t.UserId == id);
    //    }

    //    public IEnumerable<Ticket> TicketsByEvent(int id)
    //    {
    //        return _tickets.Where<Ticket>(t => t.EventId == id);
    //    }

    //    public User GetUserById(int id)
    //    {
    //        User _user = _users.FirstOrDefault<User>(u => u.Id == id);

    //        if (_user != null)
    //        {
    //            _user.Tickets = _tickets.Where<Ticket>(t => t.UserId == _user.Id).ToList<Ticket>();
    //            _user.Orders = _orders.Where<Order>(o => o.UserId == _user.Id).ToList<Order>();

    //            return _user;
    //        }

    //        return null;

    //    }

    //    public User GetUserByEmail(string email)
    //    {
    //        User _user = _users.FirstOrDefault<User>(u => u.Email == email);

    //        if (_user != null)
    //        {
    //            _user.Tickets = _tickets.Where<Ticket>(t => t.UserId == _user.Id).ToList<Ticket>();
    //            _user.Orders = _orders.Where<Order>(o => o.UserId == _user.Id).ToList<Order>();

    //            return _user;
    //        }

    //        return null;
    //    }

    //    public Task<User> GetUserByEmailAndPasswordAsync(string email, string password)
    //    {

    //        return Task<User>.Run(() =>
    //        {
    //            return _users.FirstOrDefault(u => u.Email == email && u.Password == Hash(password));
    //        });
    //    }

    //    public Task<User> GetUserByEmailAsync(string email)
    //    {
    //        return Task<User>.Run(() =>
    //        {
    //            return _users.FirstOrDefault(u => u.Email == email);
    //        });
    //    }

    //    public void AddUser(string email, string password)
    //    {

    //        _users.Add(new User { Id = _id++, Email = email, Password = Hash(password) });

    //    }

    //    public static string Hash(string pw)
    //    {
    //        byte[] _bytes = Encoding.Unicode.GetBytes(pw);

    //        using (var _md5 = MD5.Create())
    //        {
    //            var result = _md5.ComputeHash(_bytes);
    //            return Encoding.Unicode.GetString(result);
    //        }
    //    }

    //    public void UpdateUser(User user)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public IEnumerable<City> GetCities()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
