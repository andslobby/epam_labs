﻿using Microsoft.AspNetCore.Http;
using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Reselling.Domain.Infrastructure
{
    public interface IRepository
    {

        IEnumerable<Role> GetRoles();


        #region //Manage user

        IEnumerable<User> GetUsers();

        User GetUserById(int id);

        User GetUserByEmail(string email);

        Task<User> GetUserByEmailAsync(string email);

        Task<User> GetUserByEmailAndPasswordAsync(string email, string password);

        void AddUser(string email, string password);

        void UpdateUser(User user);

        void UpdateUserRole(User user);


        #endregion

        #region //Manage tickets

        Ticket GetTicketById(int id);

        void AddTicket(Ticket ticket);

        IEnumerable<Ticket> TicketsByUser(int id);

        IEnumerable<Ticket> TicketsByEvent(int id);

        #endregion

        #region //Manage orders

        IEnumerable<Order> OrdersByUser(int id);



        void AddOrder(Order order);



        Order GetOrderByTicketId(int id);
        
        void AddTrackingNumberToOrder(int id, int number);

        void AddErrorMesageToOrder(int id, string message);

        #endregion

        #region //Manage TicketStatus

        TicketStatus GetTicketStatusByName(string name);

        void SetTicketStatus(int id, TicketStatusEnum ts);


        #endregion

        #region //Manage OrderStatus

        OrderStatus GetOrderStatusByName(string name);
        
        void SetOrderStatus(int id, OrderStatusEnum os);


        #endregion



        #region //Manage Cities

        IEnumerable<City> GetCities();

        City GetCityById(int id);

        City GetCityByName(string name);

        void UpdateCity(City city);

        void AddCity(City city);

        void DeleteCityById(int id);

        #endregion

        #region //Manage Venues

        IEnumerable<Venue> GetVenues();

        Venue GetVenueById(int id);

        Venue GetVenueByName(string name);

        void UpdateVenue(Venue venue);

        void AddVenue(Venue venue);

        void DeleteVenueById(int id);

        #endregion

        #region //Manage Events

        IEnumerable<Event> GetEvents();

        Event GetEventById(int id);

        Event GetEventByName(string name);

        void UpdateEvent(Event ev);

        void AddEvent(Event ev);

        void DeleteEventById(int id);

        void SaveBanner(IFormFile file);

        #endregion



    }
}
