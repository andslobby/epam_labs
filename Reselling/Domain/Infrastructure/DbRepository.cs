﻿using System;
using System.Collections.Generic;
using System.Text;
using Reselling.Domain.Models.Authentication;
using Reselling.Domain.Models.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;

namespace Reselling.Domain.Infrastructure
{
    public class DbRepository : IRepository
    {
        DataContext _dataContext;
        private IHostingEnvironment hostingEnv;

        public DbRepository(DataContext context, IHostingEnvironment env)
        {
            _dataContext = context;
            hostingEnv = env;
        }

        public IEnumerable<Role> GetRoles()
        {
            IEnumerable<Role> _roles = _dataContext.Roles;

            return _roles;
        }

        #region// Manage tickets

        public Ticket GetTicketById(int id)
        {
            return _dataContext.Tickets.FirstOrDefault<Ticket>(t => t.Id == id);
        }

        public void AddTicket(Ticket ticket)
        {
            _dataContext.Tickets.Add(ticket);

            _dataContext.SaveChanges();
        }

        public IEnumerable<Ticket> TicketsByUser(int id)
        {
            return _dataContext.Tickets.Where<Ticket>(t => t.UserId == id);
        }

        public IEnumerable<Ticket> TicketsByEvent(int id)
        {
            return _dataContext.Tickets.Where<Ticket>(t => t.EventId == id);
        }

        #endregion

        #region//Manage orders
        public void AddOrder(Order order)
        {
            _dataContext.Orders.Add(order);
            _dataContext.SaveChanges();
        }


        public IEnumerable<Order> OrdersByUser(int id)
        {
            return _dataContext.Orders.Where<Order>(o => o.UserId == id); ;
        }

        Order GetOrderById(int id)
        {
            Order _order = _dataContext.Orders.FirstOrDefault(o => o.Id == id);

            return _order;
        }

        public Order GetOrderByTicketId(int id)
        {
            return _dataContext.Orders.FirstOrDefault(o => o.TicketId == id);
        }

        public void AddTrackingNumberToOrder(int id, int number)
        {
            Order _order = GetOrderById(id);
            if (_order != null)
                _order.TrackingNumber = number;
            _dataContext.Orders.Update(_order);
            _dataContext.SaveChanges();
        }

        public void AddErrorMesageToOrder(int id, string message)
        {
            Order _order = GetOrderById(id);
            if (_order != null)
                _order.RejectingMessage = message;
            _dataContext.Orders.Update(_order);
            _dataContext.SaveChanges();
        }

        #endregion

        #region //Manage TicketStatus

        public TicketStatus GetTicketStatusByName(string name)
        {
            return _dataContext.TicketStatuses.FirstOrDefault(ts => ts.Name.ToUpper() == name.ToUpper());
        }

        public void SetTicketStatus(int id, TicketStatusEnum ts)
        {
            Ticket _ticket = GetTicketById(id);
            if (_ticket != null)
            {
                _ticket.TicketStatusId = GetTicketStatusByName(ts.ToString("g")).Id;
                _dataContext.Tickets.Update(_ticket);
                _dataContext.SaveChanges();
            }
        }

        #endregion

        #region //Manage OrderStatus

        public OrderStatus GetOrderStatusByName(string name)
        {
            return _dataContext.OrderStatuses.FirstOrDefault(ts => ts.Name.ToUpper() == name.ToUpper());
        }

        public void SetOrderStatus(int id, OrderStatusEnum os)
        {
            Order _order = GetOrderById(id);
            if (_order != null)
            {
                _order.OrderStatusId = GetOrderStatusByName(os.ToString("g")).Id;
                _dataContext.Orders.Update(_order);
                _dataContext.SaveChanges();
            }
        }

        #endregion

        #region//Manage users

        public IEnumerable<User> GetUsers()
        {
            IEnumerable<User> _users = _dataContext.Users.Include(u => u.Role);

            return _users;
        }


        public User GetUserById(int id)
        {
            User _user = _dataContext.Users.Include(u => u.Role).FirstOrDefault<User>(u => u.Id == id);

            return _user;
        }

        public User GetUserByEmail(string email)
        {
            User _user = _dataContext.Users.Include(u => u.Role).Include(u => u.Tickets).Include(u => u.Orders).FirstOrDefault<User>(u => u.Email == email);
            foreach (Ticket t in _user.Tickets)
            {
                t.Event = _dataContext.Events.Include(e => e.Venue.City).FirstOrDefault(e => e.Id == t.EventId);
            }

            foreach (Order o in _user.Orders)
            {
                o.Ticket = _dataContext.Tickets.Include(t => t.Event.Venue.City).FirstOrDefault(t => t.Id == o.TicketId);
                o.OrderStatus = _dataContext.OrderStatuses.FirstOrDefault(os => os.Id == o.OrderStatusId);
            }
            return _user;
        }

        public Task<User> GetUserByEmailAndPasswordAsync(string email, string password)
        {
            return _dataContext.Users.Include(u => u.Role).FirstOrDefaultAsync(u => u.Email == email && u.Password == Security.Hash(password));
        }

        public Task<User> GetUserByEmailAsync(string email)
        {
            return _dataContext.Users.Include(u => u.Role).FirstOrDefaultAsync(u => u.Email == email);
        }

        public void AddUser(string email, string password)
        {

            _dataContext.Users.Add(new User { Email = email, Password = Security.Hash(password), RoleId = _dataContext.Roles.LastOrDefault(r => r.Name == "User").Id });

            _dataContext.SaveChanges();

        }

        public void UpdateUser(User user)
        {
            if (user != null)
            {
                User _user = GetUserById(user.Id);

                if (_user != null)

                {
                    _user.FirstName = user.FirstName;
                    _user.LastName = user.LastName;
                    _user.Country = user.Country;
                    _user.Adress = user.Adress;
                    _user.PhoneNumber = user.PhoneNumber;

                    _dataContext.Users.Update(_user);
                    _dataContext.SaveChanges();
                }
            }
        }

        public void UpdateUserRole(User user)
        {
            if (user != null)
            {
                User _user = GetUserById(user.Id);
                if (_user != null)
                {
                    _user.RoleId = user.RoleId;
                    _dataContext.Users.Update(_user);
                    _dataContext.SaveChanges();
                }
            }
        }

        #endregion

        #region//Manage cities

        public IEnumerable<City> GetCities()
        {
            List<City> _cities;

            _cities = _dataContext.Cities.ToList<City>();

            return _cities;
        }

        public City GetCityById(int id)
        {
            City _city = _dataContext.Cities.FirstOrDefault<City>(u => u.Id == id);

            return _city;

        }

        public City GetCityByName(string name)
        {
            City _city = _dataContext.Cities.FirstOrDefault<City>(u => u.Name.ToUpper() == name.ToUpper());

            return _city;

        }

        public void DeleteCityById(int id)
        {
            City _city = _dataContext.Cities.Include(c => c.Venues).FirstOrDefault(c => c.Id == id);

            if (_city != null)
            {
                if (_city.Venues != null)
                {
                    foreach (Venue v in _city.Venues)
                    {
                        v.Events = _dataContext.Events.Where(e => e.VenueId == v.Id).ToList<Event>();
                    }
                }

                _dataContext.Cities.Remove(_city);
                _dataContext.SaveChanges();
            }

        }


        public void UpdateCity(City city)
        {
            if (city != null)
            {
                City _city = GetCityById(city.Id);

                if (_city != null)

                {
                    _city.Name = city.Name;

                    _dataContext.Cities.Update(_city);

                    _dataContext.SaveChanges();
                }
            }
        }

        public void AddCity(City city)
        {
            if (city != null)
            {
                City _city = GetCityByName(city.Name);

                if (_city == null)

                {
                    _dataContext.Cities.Add(city);
                    _dataContext.SaveChanges();
                }
            }
        }

        #endregion

        #region //Manage venues


        public IEnumerable<Venue> GetVenues()
        {
            List<Venue> _venues;

            _venues = _dataContext.Venues.Include(v => v.City).ToList<Venue>();

            return _venues;
        }

        public Venue GetVenueById(int id)
        {
            Venue _venue = _dataContext.Venues.Include(v => v.City).FirstOrDefault<Venue>(u => u.Id == id);

            return _venue;

        }

        public Venue GetVenueByName(string name)
        {
            Venue _venue = _dataContext.Venues.Include(v => v.City).FirstOrDefault<Venue>(u => u.Name.ToUpper() == name.ToUpper());

            return _venue;

        }

        public void DeleteVenueById(int id)
        {

            Venue _venue = _dataContext.Venues.Include(v => v.Events).FirstOrDefault(c => c.Id == id);

            if (_venue != null)
            {
                _dataContext.Venues.Remove(_venue);
                _dataContext.SaveChanges();
            }
        }

        public void UpdateVenue(Venue venue)
        {
            if (venue != null)
            {
                Venue _venue = GetVenueById(venue.Id);

                if (_venue != null)

                {
                    _venue.Name = venue.Name;
                    _venue.Adress = venue.Adress;
                    _venue.CityId = venue.CityId;
                    _dataContext.Venues.Update(_venue);

                    _dataContext.SaveChanges();
                }
            }
        }

        public void AddVenue(Venue venue)
        {
            if (venue != null)
            {
                Venue _venue = GetVenueByName(venue.Name);

                if (_venue == null)

                {
                    _dataContext.Venues.Add(venue);
                    _dataContext.SaveChanges();
                }
            }
        }

        #endregion

        #region //Manage events

        public IEnumerable<Event> GetEvents()
        {
            List<Event> _events;

            _events = _dataContext.Events.Include(e => e.Venue).ToList<Event>();

            foreach (Event e in _events)
                e.Venue.City = _dataContext.Cities.Where(c => c.Id == e.Venue.CityId).FirstOrDefault();

            return _events;
        }


        public Event GetEventById(int id)
        {
            Event _event = _dataContext.Events.Include(e => e.Venue.City).FirstOrDefault<Event>(e => e.Id == id);
            _event.Tickets = _dataContext.Tickets.Where(t => t.EventId == _event.Id).Where(t => t.TicketStatusId == GetTicketStatusByName(TicketStatusEnum.SellingTicket.ToString("g")).Id).ToList();
          //  _event.Venue.City = _dataContext.Cities.Where(c => c.Id == _event.Venue.CityId).FirstOrDefault();
            if (_event.Tickets != null)
            {
                foreach (Ticket t in _event.Tickets)
                {
                    t.User = _dataContext.Users.FirstOrDefault(u => u.Id == t.UserId);
                }
            }
            //   _event.Tickets = _dataContext.Tickets.Where(t => t.EventId == _event.Id).ToList<Ticket>();
            return _event;
        }


        public Event GetEventByName(string name)
        {
            Event _ev = _dataContext.Events.Include(e => e.Venue).Include(e => e.Tickets).FirstOrDefault<Event>(e => e.Name.ToUpper() == name.ToUpper());

            return _ev;

        }

        public void DeleteEventById(int id)
        {
            Event _ev = new Event { Id = id };

            _dataContext.Entry(_ev).State = EntityState.Deleted;
            _dataContext.SaveChanges();

        }


        public void UpdateEvent(Event ev)
        {
            if (ev != null)
            {
                Event _ev = GetEventById(ev.Id);
                if (_ev != null)
                {
                    _ev.Name = ev.Name;
                    _ev.Date = ev.Date;
                    _ev.VenueId = ev.VenueId;
                    _ev.Banner = ev.Banner;
                    _ev.Description = ev.Description;
                    _dataContext.Events.Update(_ev);
                    _dataContext.SaveChanges();
                }
            }
        }

        public void AddEvent(Event ev)
        {
            if (ev != null)
            {
                Event _ev = GetEventByName(ev.Name);

                if (_ev == null)

                {
                    _dataContext.Events.Add(ev);
                    _dataContext.SaveChanges();
                }
            }
        }

        public void SaveBanner(IFormFile file)
        {
            string _filename = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            _filename = hostingEnv.WebRootPath + $@"\images\Movies\{_filename}";
            if (!File.Exists(_filename))
            {
                using (FileStream fs = File.Create(_filename))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }
            }

        }

        #endregion

    }
}
